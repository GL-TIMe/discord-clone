# Discord Clone

Демо приложения можно посмотреть [здесь](https://discord-clone-rho.vercel.app/)

## :memo: Полный стек:

Frontend:

- React + TypeScript
- Redux
- react-redux
- redux-thunk
- React Router

Backend:

- Firebase
- Socket.io
- simple-peer

## :rocket: Запуск

Перед тем как запустить проект необходимо установить все зависимости с помощью команды:

```
yarn install
```

После чего, для запуска проекта вы можете написать команду:

```
yarn start
```
