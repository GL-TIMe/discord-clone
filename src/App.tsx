import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { AddServerModal } from "./components/AddServerModal";

import AppRouter from "./components/AppRouter";
import { Loader } from "./components/Loader";

function App() {
  return (
    <Router>
      <Loader />
      <AddServerModal />

      <AppRouter />
    </Router>
  );
}

export default App;
