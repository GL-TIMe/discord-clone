import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/auth";
import { User } from "./redux/types";
import { throttle } from "./helpers/throttle";

if (!firebase.apps.length) {
  firebase.initializeApp({
    apiKey: "AIzaSyBYHia9c7MzkBtbb7UxNiFdQW4WHb8Yd4o",
    authDomain: "discord-clone-edb71.firebaseapp.com",
    projectId: "discord-clone-edb71",
    storageBucket: "discord-clone-edb71.appspot.com",
    messagingSenderId: "215284842454",
    appId: "1:215284842454:web:b87cfcb83a3d38ab4dee77",
  });
}

const db = firebase.firestore();
const auth = firebase.auth();

const addMessage = (
  user: User,
  serverUID: string,
  chatUID: string,
  message: string
) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("chats")
    .doc(chatUID)
    .collection("messages")
    .add({
      uid: user.uid,
      displayName: user.displayName,
      text: message,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    });
};

const addChannel = throttle(async (serverUID: string, title: string) => {
  return await db.collection("servers").doc(serverUID).collection("chats").add({
    createdAt: firebase.firestore.FieldValue.serverTimestamp(),
    title,
  });
}, 1000);

const addChannelThrottled = async (serverUID: string, title: string) =>
  await addChannel(serverUID, title);

const addVoice = throttle(async (serverUID: string, title: string) => {
  return await db
    .collection("servers")
    .doc(serverUID)
    .collection("voices")
    .add({
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      title,
    });
}, 1000);

const addVoiceThrottled = async (serverUID: string, title: string) =>
  await addVoice(serverUID, title);

const updateVoiceMembers = (
  serverUID: string,
  voiceUID: string,
  user: User
) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("voices")
    .doc(voiceUID)
    .collection("members")
    .doc(user.uid)
    .set({
      displayName: user.displayName,
      photoURL: user.photoURL,
    });
};

const leaveFromVoice = (
  serverUID: string,
  voiceUID: string,
  userUID: string
) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("voices")
    .doc(voiceUID)
    .collection("members")
    .doc(userUID)
    .delete();
};

const setOfflineUser = (serverUID: string, user: User) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("members")
    .doc(user.uid)
    .update({
      status: "offline",
    });
};

const setOnlineUser = (serverUID: string, user: User) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("members")
    .doc(user.uid)
    .update({
      status: "online",
    });
};

export {
  db,
  auth,
  addMessage,
  addChannelThrottled,
  addVoiceThrottled,
  setOfflineUser,
  setOnlineUser,
  updateVoiceMembers,
  leaveFromVoice,
};
