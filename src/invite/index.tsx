import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { RouteComponentProps, useHistory } from "react-router-dom";

import { AppThunkDispatch, RootState } from "../redux/store";
import {
  checkServerExistThrottled,
  getChatUID,
  inviteUserToServerThrottled,
} from "../redux/actions/user";

import { InviteServerNotFound } from "./component/InviteServerNotFound";
import { AuthInvite } from "./component/AuthInvite";
import { hideLoader, showLoader } from "../redux/actions/modals";
import { User } from "../redux/types";

type MatchParams = {
  serverUID: string;
};

interface MatchProps extends RouteComponentProps<MatchParams> {}

type InviteProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> &
  MatchProps;

const Invite: React.FC<InviteProps> = ({
  match,
  user,
  showLoader,
  hideLoader,
  inviteUserToServerThrottled,
}) => {
  const { serverUID } = match.params;
  const [error, setError] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(true);

  const history = useHistory();

  useEffect(() => {
    setLoading(true);
    showLoader();
    checkServerExistThrottled(serverUID).then((isExist) => {
      if (!isExist) {
        setError(true);
        setLoading(false);
        hideLoader();
        return;
      }

      if (!Object.keys(user).length) {
        setLoading(false);
        hideLoader();
        return;
      }

      if (!user.servers.includes(serverUID)) {
        inviteUserToServerThrottled(user, serverUID).then((chatUID) => {
          setLoading(false);
          hideLoader();
          history.push(`/channels/${serverUID}/${chatUID}`);
        });
      } else {
        getChatUID(serverUID).then((chatUID) => {
          setLoading(false);
          hideLoader();
          history.push(`/channels/${serverUID}/${chatUID}`);
        });
      }
    });
  }, [
    user,
    serverUID,
    showLoader,
    hideLoader,
    inviteUserToServerThrottled,
    history,
  ]);

  if (loading) {
    return null;
  }

  if (error) {
    return <InviteServerNotFound />;
  }

  if (!Object.keys(user).length) {
    return <AuthInvite serverUID={serverUID} />;
  }

  return null;
};

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  showLoader: () => dispatch(showLoader),
  hideLoader: () => dispatch(hideLoader),
  inviteUserToServerThrottled: (user: User, serverUID: string) =>
    dispatch(inviteUserToServerThrottled(user, serverUID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Invite);
