import React, { useEffect, useState } from "react";
import { Switch, Route, Link } from "react-router-dom";

import { ServerInfoHeader } from "../../components/ServerInfoHeader";
import { LoginForm } from "../../components/LoginForm";
import { RegisterForm } from "../../components/RegisterForm";

import { db } from "../../api";
import { Member, Server } from "../../redux/types";
import { ModalLoader } from "../../components/ModalLoader";

type AuthInviteType = {
  serverUID: string;
};

const AuthInvite: React.FC<AuthInviteType> = ({ serverUID }) => {
  const [server, setServer] = useState<Server | null>(null);
  const [members, setMembers] = useState<Member[] | null>(null);

  useEffect(() => {
    const serverRef = db.collection("servers").doc(serverUID);

    serverRef.get().then((doc) => {
      setServer(doc.data() as Server);
      serverRef
        .collection("members")
        .get()
        .then((members) =>
          setMembers(members.docs.map((member) => member.data() as Member))
        );
    });
  }, [serverUID]);

  return server && members ? (
    <nav className="auth__container">
      <div className="auth__box">
        <ServerInfoHeader server={server!} members={members!} />

        <Switch>
          <Route
            path={["/invite/:serverUID", "/invite/:serverUID/login"]}
            component={LoginForm}
            exact
          />
          <Route
            path="/invite/:serverUID/register"
            component={RegisterForm}
            exact
          />
        </Switch>

        <Switch>
          <Route
            path={["/invite/:serverUID", "/invite/:serverUID/login"]}
            render={() => (
              <footer className="auth__footer">
                <p className="auth__text">
                  Нужна учётная запись?{" "}
                  <Link
                    className="auth__link"
                    to={`/invite/${serverUID}/register`}
                  >
                    Зарегистрироваться
                  </Link>
                </p>
              </footer>
            )}
            exact
          />
          <Route
            path="/invite/:serverUID/register"
            render={() => (
              <footer className="auth__footer">
                <Link to={`/invite/${serverUID}/login`} className="auth__link">
                  Уже зарегистрированы?
                </Link>
              </footer>
            )}
            exact
          />
        </Switch>
      </div>
    </nav>
  ) : (
    <ModalLoader />
  );
};

export { AuthInvite };
