import React from "react";
import { useHistory } from "react-router";
import { Button } from "../../components/Button";

import notFound from "../../img/not-found.svg";

const InviteServerNotFound = () => {
  const history = useHistory();

  const handleSubmit = () => {
    history.push("/");
  };

  return (
    <section className="modal">
      <form className="modal__container" onSubmit={handleSubmit}>
        <header className="modal__header">
          <img src={notFound} alt="invite not available" />
          <h2 className="modal__title">Приглашение не действительно</h2>
          <h3 className="modal__subtitle">
            Возможно указанный сервер не существует
          </h3>
        </header>
        <footer className="modal__content">
          <Button type="submit">Продолжить в Discord Clone</Button>
        </footer>
      </form>
    </section>
  );
};

export { InviteServerNotFound };
