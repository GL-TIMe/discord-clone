import React from "react";

import loaderImg from "../img/loader.png";
import loaderWEBM from "../video/loader.webm";
import loaderMP4 from "../video/loader.mp4";
import { useAppSelector } from "../redux/hook";

export const Loader = () => {
  const { loader } = useAppSelector((state) => state.modals);

  return loader ? (
    <div className="loader">
      <div className="loader__container">
        {/* browsers can only autoplay the videos with sound off (muted) */}
        <video className="loader__video" loop autoPlay playsInline muted>
          <source src={loaderWEBM} type="video/webm" />
          <source src={loaderMP4} type="video/mp4" />
          <img src={loaderImg} alt="loader" />
        </video>
        <p className="loader__text">Пожалуйста, подождите...</p>
      </div>
    </div>
  ) : null;
};
