import React from "react";

import loader from "../img/loader.png";

const ModalLoader: React.FC = () => {
  return (
    <section className="modal">
      <div className="modal__container">
        <header className="modal__header">
          <h2 className="modal__title">Пожалуйста, подождите...</h2>
          <img className="modal__img" src={loader} alt="loader" />
        </header>
      </div>
    </section>
  );
};

export { ModalLoader };
