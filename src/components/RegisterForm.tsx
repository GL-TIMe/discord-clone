import React, { useState } from "react";
import { useHistory } from "react-router-dom";

import { Button } from "./Button";
import { InputField } from "./InputField";

import { signUpThrottled } from "../redux/actions/user";
import { useThunkDispatch } from "../redux/hook";

import { Routes } from "../routes";
import { useInput } from "../validation";

export const RegisterForm = () => {
  const dispatch = useThunkDispatch();
  const history = useHistory();

  const [loaded, setLoaded] = useState<boolean>(true);

  const email = useInput("", { isEmpty: true });
  const username = useInput("", { isEmpty: true, isUsername: true });
  const password = useInput("", {
    isEmpty: true,
    isPassword: true,
  });

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoaded(false);
    const isEmailDirty = email.onSubmit();
    const isUsernameDirty = username.onSubmit();
    const isPasswordDirty = password.onSubmit();
    if (!isEmailDirty && !isUsernameDirty && !isPasswordDirty) {
      try {
        await dispatch(
          signUpThrottled(username.value, email.value, password.value)
        );

        history.push(Routes.SERVERS_NOT_FOUND_ROUTE);
      } catch (error) {
        setLoaded(true);
        switch (error.code) {
          case "auth/email-already-in-use":
          case "auth/invalid-email":
            email.setDirty(true);
            email.setErrorMessage(error.message);
            break;
          default:
            email.setDirty(true);
            email.setErrorMessage(error.message);
            username.setDirty(true);
            username.setErrorMessage(error.message);
            password.setDirty(true);
            password.setErrorMessage(error.message);
        }
      }
    } else {
      setLoaded(true);
    }
  };

  return (
    <form onSubmit={handleSubmit} autoComplete="nope">
      {/* skip name, because autoComplete will not work correctly */}
      <InputField
        className="auth__input"
        label="Имя пользователя"
        type="text"
        value={username.value}
        onChange={username.onChange}
        error={username.isDirty}
        errorMessage={username.errorMessage}
        autoComplete="off"
      />
      <InputField
        className="auth__input"
        label="Адрес электронной почты"
        type="text"
        value={email.value}
        onChange={email.onChange}
        error={email.isDirty}
        errorMessage={email.errorMessage}
        autoComplete="off"
      />
      <InputField
        className="auth__input"
        label="Пароль"
        type="password"
        value={password.value}
        onChange={password.onChange}
        error={password.isDirty}
        errorMessage={password.errorMessage}
        autoComplete="off"
      />
      <Button loaded={loaded} type="submit">
        Продолжить
      </Button>
    </form>
  );
};
