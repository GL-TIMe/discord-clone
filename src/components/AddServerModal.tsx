import React, { useState } from "react";
import { Button } from "./Button";
import { InputField } from "./InputField";
import { CloseButton } from "../icons";
import { closeServerModal } from "../redux/actions/modals";
import { useAppSelector, useThunkDispatch } from "../redux/hook";
import { useHistory } from "react-router";
import {
  checkServerExistThrottled,
  getChatUID,
  inviteUserToServerThrottled,
} from "../redux/actions/user";

const LINK_REGEXP = new RegExp(
  "https://discord-clone-rho.vercel.app/invite/([a-zA-Z0-9]+)"
);

const AddServerModal = () => {
  const [disabled, setDisabled] = useState<boolean>(true);
  const [inviteLink, setInviteLink] = useState<string>("");
  const [isDirty, setDirty] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [loaded, setLoaded] = useState<boolean>(true);

  const history = useHistory();

  const { server } = useAppSelector((state) => state.modals);
  const { user } = useAppSelector((state) => state.user);
  const dispatch = useThunkDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInviteLink(e.target.value);

    if (e.target.value) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    const trimInviteLink = inviteLink.trim();
    const matchResult = trimInviteLink.match(LINK_REGEXP);
    const serverCode = (matchResult && matchResult[1]) || trimInviteLink;

    if (serverCode) {
      if (user.servers.includes(serverCode)) {
        const defaultChatUID = await getChatUID(serverCode);
        handleClose();
        history.push(`/channels/${serverCode}/${defaultChatUID}`);
        return;
      }

      setLoaded(false);

      const isExist = await checkServerExistThrottled(serverCode);

      if (isExist) {
        const defaultChatUID = await dispatch(
          inviteUserToServerThrottled(user, serverCode)
        );
        handleClose();

        history.push(`/channels/${serverCode}/${defaultChatUID}`);
      } else {
        setDirty(true);
        setLoaded(true);
        setError("Приглашение содерджит ошибку");
      }
    } else {
      setDirty(true);
      setError(
        "Пожалуйства введите действительный код или ссылку-приглашение!"
      );
    }
  };

  const handleClose = () => {
    dispatch(closeServerModal);
    setInviteLink("");
    setDisabled(true);
    setDirty(false);
    setLoaded(true);
  };

  return server ? (
    <section className="modal">
      <form className="modal__container" onSubmit={handleSubmit}>
        <header className="modal__header">
          <h2 className="modal__title">Присоединиться к серверу</h2>
          <h3 className="modal__subtitle">
            Введите приглашение, чтобы присоединиться к существующему серверу.
          </h3>
          <button className="modal__close" type="button" onClick={handleClose}>
            <CloseButton className="modal__close-icon" width={24} height={24} />
          </button>
        </header>

        <main className="modal__content">
          <InputField
            type="text"
            label="ссылка-приглашение"
            value={inviteLink}
            error={isDirty}
            errorMessage={error}
            onChange={handleChange}
          />
        </main>

        <div className="modal__example">
          <h3 className="text-field__label">
            Приглашения должны выглядить так:
          </h3>
          <div className="modal__text">24bkp24uU9GgfzmGqHfW</div>
          <div className="modal__text">
            https://discord-clone-rho.vercel.app/invite/24bkp24uU9GgfzmGqHfW
          </div>
        </div>

        <footer className="modal__footer">
          <Button
            className="modal__button modal__button_outline"
            onClick={handleClose}
          >
            <div className="modal__button-content">Отмена</div>
          </Button>
          <Button
            className="modal__button"
            type="submit"
            loaded={loaded}
            disabled={disabled}
          >
            <div className="modal__button-content">Присоедиться к серверу</div>
          </Button>
        </footer>
      </form>
    </section>
  ) : null;
};

export { AddServerModal };
