import clsx from "clsx";
import React from "react";

type InputFieldProps = {
  type: string;
  className?: string;
  id?: string;
  name?: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  label?: string;
  autoComplete?: string;
  error?: boolean;
  errorMessage?: string;
};

const InputField: React.FC<InputFieldProps> = ({
  label,
  className,
  id,
  name,
  type,
  value,
  onChange,
  autoComplete = "off",
  error,
  errorMessage,
}) => {
  return (
    <div className={className}>
      <label className={clsx("text-field__label", { error: error })}>
        {label}
        <span className="text-field__error">
          {error ? ` - ${errorMessage}` : null}
        </span>
      </label>
      <input
        className={clsx("text-field__input", { error: error })}
        id={id}
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        autoComplete={autoComplete}
      />
    </div>
  );
};

export { InputField };
