import clsx from "clsx";
import React from "react";

type ButtonProps = {
  className?: string;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  loaded?: boolean;
  disabled?: boolean;
  type?: "button" | "submit" | "reset" | undefined;
};

const Button: React.FC<ButtonProps> = ({
  className,
  type = "button",
  onClick,
  loaded = true,
  disabled = false,
  children,
}) => {
  return (
    <button
      className={clsx("button", { button__loading: !loaded }, className)}
      type={type}
      disabled={disabled}
      onClick={onClick}
    >
      {!loaded ? (
        <span className="dot-flashing">
          <span className="dot-flashing__item"></span>
          <span className="dot-flashing__item"></span>
          <span className="dot-flashing__item"></span>
        </span>
      ) : null}
      <span className="button__content">{children}</span>
    </button>
  );
};

export { Button };
