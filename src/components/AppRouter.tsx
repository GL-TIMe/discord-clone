import React from "react";
import { connect } from "react-redux";
import { Route, Switch, Redirect } from "react-router-dom";
import { ChannelsRedirect } from "../servers/components/ChannelsRedirect";

import { RootState } from "../redux/store";
import { commonRoutes, privateRoutes, publicRoutes, Routes } from "../routes";
import AudioList from "./AudioList";

type AppRouterProps = ReturnType<typeof mapStateToProps>;

const AppRouter: React.FC<AppRouterProps> = ({ user }) => {
  return (
    <>
      {Object.keys(user).length ? (
        <>
          <AudioList />
          <Switch>
            {commonRoutes.map(({ path, Component }) => (
              <Route key={path} path={path} component={Component} />
            ))}
            {privateRoutes.map(({ path, Component }) => (
              <Route key={path} path={path} component={Component} />
            ))}
            <Route path="*" component={ChannelsRedirect} exact />
          </Switch>
        </>
      ) : (
        <Switch>
          {commonRoutes.map(({ path, Component }) => (
            <Route key={path} path={path} component={Component} />
          ))}
          {publicRoutes.map(({ path, Component }) => (
            <Route key={path} path={path} component={Component} exact />
          ))}
          <Redirect to={Routes.LOGIN_ROUTE} />
        </Switch>
      )}
    </>
  );
};

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
});

export default connect(mapStateToProps)(AppRouter);
