import React from "react";

import { Member, Server } from "../redux/types";

type ServerInfoHeaderProps = {
  server: Server;
  members: Member[];
};

export const ServerInfoHeader: React.FC<ServerInfoHeaderProps> = ({
  server,
  members,
}) => {
  return (
    <header className="auth__server">
      <h3 className="auth__subtitle auth__server-subtitle">
        Вас приглашают на сервер
      </h3>
      <div className="auth__server-info">
        <div className="person__avatar">
          <img
            className="person__img"
            src={server.photoURL}
            alt="server avatar"
          />
        </div>
        <h2 className="auth__title">{server.title}</h2>
      </div>
      <div className="auth__server-members">
        <div className="auth__server-member">
          <i className="auth__server-icon auth__server-icon_online"></i>
          <span className="auth__server-text">
            {members.filter(({ status }) => status === "online").length} Online
          </span>
        </div>
        <div className="auth__server-member">
          <i className="auth__server-icon auth__server-icon_offline"></i>
          <span className="auth__server-text">{members.length} Members</span>
        </div>
      </div>
    </header>
  );
};
