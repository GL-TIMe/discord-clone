import React, { useState } from "react";
import { Button } from "./Button";
import { InputField } from "./InputField";
import { signInThrottled } from "../redux/actions/user";
import { useThunkDispatch } from "../redux/hook";
import { useInput } from "../validation";

const LoginForm: React.FC = () => {
  const dispatch = useThunkDispatch();

  const [loaded, setLoaded] = useState<boolean>(true);

  const email = useInput("", { isEmpty: true });
  const password = useInput("", { isEmpty: true });

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    setLoaded(false);
    const isEmailDirty = email.onSubmit();
    const isPasswordDirty = password.onSubmit();
    if (!isEmailDirty && !isPasswordDirty) {
      try {
        await dispatch(signInThrottled(email.value, password.value));
      } catch (error) {
        setLoaded(true);
        switch (error.code) {
          case "auth/invalid-email":
          case "auth/user-not-found":
            email.setDirty(true);
            email.setErrorMessage(error.message);
            break;
          case "auth/wrong-password":
            password.setDirty(true);
            password.setErrorMessage(error.message);
            break;
          default:
            email.setDirty(true);
            email.setErrorMessage(error.message);
            password.setDirty(true);
            password.setErrorMessage(error.message);
        }
      }
    } else {
      setLoaded(true);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <InputField
        className="auth__input"
        label="Адрес электронной почты"
        type="text"
        name="email"
        error={email.isDirty}
        errorMessage={email.errorMessage}
        value={email.value}
        onChange={email.onChange}
        autoComplete="username"
      />
      <InputField
        className="auth__input"
        label="Пароль"
        type="password"
        name="password"
        error={password.isDirty}
        errorMessage={password.errorMessage}
        value={password.value}
        onChange={password.onChange}
        autoComplete="current-password"
      />
      <Button loaded={loaded} type="submit">
        Вход
      </Button>
    </form>
  );
};

export { LoginForm };
