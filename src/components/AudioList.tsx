import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import Peer from "simple-peer";
import { leaveFromVoice } from "../api";
import { clearConnectionInfo, setTalkingStatus } from "../redux/actions/peers";
import { AppDispatch, RootState } from "../redux/store";
import { PeersType } from "../redux/types";
import socket, { addPeer, createPeer } from "../socket";

type AudioProps = {
  peer: Peer.Instance;
  peerID: string;
};

const Audio: React.FC<AudioProps> = ({ peer, peerID }) => {
  const ref = useRef<HTMLAudioElement>(null);

  useEffect(() => {
    peer.on("stream", (stream: MediaStream) => {
      if (ref.current) {
        ref.current.srcObject = stream;
      }
    });
  }, [peer]);

  return <audio playsInline autoPlay ref={ref} arial-label={`${peerID}`} />;
};

declare global {
  interface Window {
    localStream: MediaStream;
    peersRef: PeersType[];
    setPeers: React.Dispatch<React.SetStateAction<PeersType[]>>;
  }
}

type AudioListType = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const AudioList: React.FC<AudioListType> = ({
  isTalking,
  connectionVoice,
  connectionServer,
  clearConnectionInfo,
  setTalkingStatus,
  userUID,
}) => {
  const userAudio = useRef<HTMLAudioElement>(null);
  const peersRef = useRef<PeersType[]>([]);

  const [peers, setPeers] = useState<PeersType[]>([]);

  window.setPeers = setPeers;

  useEffect(() => {
    const putDown = (e: BeforeUnloadEvent) => {
      e.preventDefault();

      if (isTalking) {
        leaveFromVoice(connectionServer, connectionVoice, userUID);
        socket.emit("leave");
      }
    };

    window.addEventListener("beforeunload", putDown, false);

    return () => {
      if (isTalking) {
        leaveFromVoice(connectionServer, connectionVoice, userUID);
        window.localStream.getTracks().forEach((track) => track.stop());
        socket.removeAllListeners();
        socket.emit("leave");
        clearConnectionInfo();
        setTalkingStatus(false);

        window.removeEventListener("beforeunload", putDown, false);
      }
    };
    // eslint-disable-next-line
  }, [isTalking]);

  useEffect(() => {
    if (isTalking) {
      peersRef.current = [];
      navigator.mediaDevices
        .getUserMedia({ video: false, audio: true })
        .then((stream) => {
          if (userAudio.current) {
            userAudio.current.srcObject = stream;
          }

          window.localStream = stream;
          window.peersRef = peersRef.current;

          socket.emit("join room", connectionVoice, userUID);

          socket.on("all users", (users: string[]) => {
            const peers: PeersType[] = [];
            users.forEach((userID) => {
              const peer = createPeer(userID, socket.id, stream);
              const user = {
                peerID: userID,
                peer,
              };

              peers.push(user);
              peersRef.current.push(user);
            });

            setPeers(peers);
          });

          socket.on(
            "user joined",
            (payload: { signal: Peer.SignalData; callerID: string }) => {
              const item = peersRef.current.find(
                (p) => p.peerID === payload.callerID
              );

              if (!item) {
                const peer = addPeer(payload.signal, payload.callerID, stream);

                peersRef.current.push({
                  peerID: payload.callerID,
                  peer,
                });

                setPeers((peers) => [
                  ...peers,
                  {
                    peerID: payload.callerID,
                    peer,
                  },
                ]);
              }
            }
          );

          socket.on("user disconnected", (userID: string) => {
            peersRef.current = peersRef.current.filter(({ peerID, peer }) => {
              if (peerID === userID) {
                peer.destroy();
                return false;
              }
              return true;
            });

            setPeers((peers) =>
              peers.filter(({ peerID }) => peerID !== userID)
            );
          });

          socket.on(
            "receiving returned signal",
            (payload: { id: string; signal: Peer.SignalData }) => {
              const item = peersRef.current.find(
                (p) => p.peerID === payload.id
              );
              item!.peer.signal(payload.signal);
            }
          );
        });
    }

    // eslint-disable-next-line
  }, [isTalking, connectionVoice]);

  return (
    <>
      <audio muted playsInline autoPlay ref={userAudio} />
      {peers.map(({ peer, peerID }, index) => (
        <Audio key={index} peer={peer} peerID={peerID} />
      ))}
    </>
  );
};

const mapStateToProps = (state: RootState) => ({
  isTalking: state.peers.isTalking,
  connectionServer: state.peers.connectionServer,
  connectionVoice: state.peers.connectionVoice,
  userUID: state.user.user.uid,
});

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  clearConnectionInfo: () => dispatch(clearConnectionInfo),
  setTalkingStatus: (status: boolean) => dispatch(setTalkingStatus(status)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AudioList);
