import Peer from "simple-peer";

export enum Constants {
  SET_CHATS = "SET_CHATS",
  SET_CURRENT_CHAT = "SET_CURRENT_CHAT",

  SET_VOICES = "SET_VOICES",

  SET_MESSAGES = "SET_MESSAGES",

  SET_SERVERS = "SET_SERVERS",
  SET_CURRENT_SERVER = "SET_CURRENT_SERVER",

  SET_USER = "SET_USER",
  UPDATE_SERVERS = "UPDATE_SERVERS",

  CLEAR_USER = "CLEAR_USER",

  SET_MEMBERS = "SET_MEMBERS",
  TOGGLE_SHOW_MEMBERS = "TOGGLE_SHOW_MEMBERS",

  SHOW_CHANNEL_MODAL = "SHOW_CHANNEL_MODAL",
  CLOSE_CHANNEL_MODAL = "CLOSE_CHANNEL_MODAL",
  SHOW_SERVER_MODAL = "SHOW_SERVER_MODAL",
  CLOSE_SERVER_MODAL = "CLOSE_SERVER_MODAL",
  SHOW_LOADER = "SHOW_LOADER",
  HIDE_LOADER = "HIDE_LOADER",
  SHOW_VOICE_MODAL = "SHOW_VOICE_MODAL",
  CLOSE_VOICE_MODAL = "CLOSE_VOICE_MODAL",

  SET_PEERS = "SET_PEERS",
  SET_CONNECTION_INFO = "SET_CONNECTION_INFO",
  SET_TALKING_STATUS = "SET_TALKING_STATUS",
  CLEAR_CONNECTION_INFO = "CLEAR_CONNECTION_INFO",
}

// messages
export type Message = {
  createdAt: {
    seconds: number;
  };
  displayName: string;
  uid: string;
  text: string;
};

export type Chat = {
  title: string;
  createdAt: {
    seconds: number;
  };
  uid: string;
};

export type SetMessagesAction = {
  type: Constants.SET_MESSAGES;
  payload: Message[];
};

export type MessagesActionType = SetMessagesAction;

// chats
export type SetChats = {
  type: Constants.SET_CHATS;
  payload: Chat[];
};

export type SetCurrentChat = {
  type: Constants.SET_CURRENT_CHAT;
  payload: Chat;
};

export type ChatsActionType = SetChats | SetCurrentChat;

// voices

export type Voice = {
  uid: string;
  title: string;
  createdAt: {
    seconds: number;
  };
  members: string[];
};

type SetVoices = {
  type: Constants.SET_VOICES;
  payload: Voice[];
};

export type VoicesActionType = SetVoices;

// user
export type User = {
  uid: string;
  photoURL: string;
  servers: string[];
  displayName: string;
};

export type SetUser = {
  type: Constants.SET_USER;
  payload: User;
};

type ClearUser = {
  type: Constants.CLEAR_USER;
};

type UpdateServersList = {
  type: Constants.UPDATE_SERVERS;
  payload: string[];
};

export type UserActionType = SetUser | ClearUser | UpdateServersList;

// servers
export type Server = {
  uid: string;
  title: string;
  defaultChat: string;
  ownerUID: string;
  photoURL: string;
  createdAt: {
    seconds: number;
  };
};

export type SetServers = {
  type: Constants.SET_SERVERS;
  payload: Server[];
};

export type SetCurrentServer = {
  type: Constants.SET_CURRENT_SERVER;
  payload: Server;
};

export type ServersActionType = SetServers | SetCurrentServer;

// members

export type Member = {
  displayName: string;
  photoURL: string;
  status: string;
  uid: string;
};

type SetMembers = {
  type: Constants.SET_MEMBERS;
  payload: Member[];
};

type ToggleShowMembers = {
  type: Constants.TOGGLE_SHOW_MEMBERS;
};

export type MembersActionType = SetMembers | ToggleShowMembers;

// modal

type ShowChannelModal = {
  type: Constants.SHOW_CHANNEL_MODAL;
};

type CloseChannelModal = {
  type: Constants.CLOSE_CHANNEL_MODAL;
};

type ShowServerModal = {
  type: Constants.SHOW_SERVER_MODAL;
};

type CloseServerModal = {
  type: Constants.CLOSE_SERVER_MODAL;
};

type ShowLoader = {
  type: Constants.SHOW_LOADER;
};

type CloseLoader = {
  type: Constants.HIDE_LOADER;
};

type ShowVoiceModal = {
  type: Constants.SHOW_VOICE_MODAL;
};

type CloseVoiceModal = {
  type: Constants.CLOSE_VOICE_MODAL;
};

export type ModalsActionType =
  | ShowServerModal
  | CloseServerModal
  | ShowChannelModal
  | CloseChannelModal
  | ShowLoader
  | CloseLoader
  | ShowVoiceModal
  | CloseVoiceModal;

// peers

export type PeersType = {
  peerID: string;
  peer: Peer.Instance;
};

type SetConnectionInfo = {
  type: Constants.SET_CONNECTION_INFO;
  payload: {
    connectionServer: Server;
    connectionVoice: Voice;
  };
};

type SetTalkingStatus = {
  type: Constants.SET_TALKING_STATUS;
  payload: boolean;
};

type ClearConnectionInfo = {
  type: Constants.CLEAR_CONNECTION_INFO;
};

export type PeersActionType =
  | SetConnectionInfo
  | SetTalkingStatus
  | ClearConnectionInfo;
