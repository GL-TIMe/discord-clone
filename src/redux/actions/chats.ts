import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { db } from "../../api";
import { RootState } from "../store";
import { Chat, ChatsActionType, Constants } from "../types";

const setChats = (chats: Chat[]): ChatsActionType => ({
  type: Constants.SET_CHATS,
  payload: chats,
});

const setCurrentChat = (chat: Chat): ChatsActionType => ({
  type: Constants.SET_CURRENT_CHAT,
  payload: chat,
});

const subscribeChats = (
  uid: string
): ThunkAction<() => void, RootState, unknown, AnyAction> => (dispatch) => {
  const unsubscribe = db
    .collection("servers")
    .doc(uid)
    .collection("chats")
    .orderBy("createdAt")
    .onSnapshot((snapshot) => {
      dispatch(
        setChats(
          snapshot.docs.map((doc) => ({ uid: doc.id, ...doc.data() })) as Chat[]
        )
      );
    });
  return unsubscribe;
};

const initialCurrentChat = (
  serverUID: string,
  chatUID: string
): ThunkAction<void, RootState, unknown, AnyAction> => (dispatch) => {
  db.collection("servers")
    .doc(serverUID)
    .collection("chats")
    .doc(chatUID)
    .get()
    .then((doc) =>
      dispatch(setCurrentChat({ uid: doc.id, ...doc.data() } as Chat))
    );
};

export { subscribeChats, initialCurrentChat };
