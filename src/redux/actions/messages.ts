import firebase from "firebase";
import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { db } from "../../api";
import { RootState } from "../store";
import { Message, MessagesActionType, Constants } from "../types";

const setMessages = (messages: Message[]): MessagesActionType => ({
  type: Constants.SET_MESSAGES,
  payload: messages,
});

const subscribeMessages = (
  serverUID: string,
  chatUID: string
): ThunkAction<() => void, RootState, unknown, AnyAction> => (dispatch) => {
  const messageQuery = db
    .collection("servers")
    .doc(serverUID)
    .collection("chats")
    .doc(chatUID)
    .collection("messages")
    .orderBy("createdAt");

  const unsubscribe = messageQuery.onSnapshot((snapshot) => {
    let messages: Message[] = [];
    snapshot.forEach((doc) => {
      const data = doc.data() as Message;

      if (!data.createdAt && doc.metadata.hasPendingWrites) {
        messages.push({
          ...data,
          uid: doc.id,
          createdAt: firebase.firestore.Timestamp.now(),
        } as Message);
      } else {
        messages.push({
          ...data,
          uid: doc.id,
        } as Message);
      }
    });
    dispatch(setMessages(messages));
  });

  return unsubscribe;
};

export { subscribeMessages, setMessages };
