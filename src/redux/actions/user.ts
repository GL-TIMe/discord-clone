import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { auth, db } from "../../api";
import { throttle } from "../../helpers/throttle";
import { AppDispatch, AppThunkDispatch, RootState } from "../store";
import { Constants, Server, User, UserActionType } from "../types";
import { showLoader } from "./modals";
import { initialServers } from "./servers";

const setUser = (user: User): UserActionType => ({
  type: Constants.SET_USER,
  payload: user,
});

const clearUser: UserActionType = {
  type: Constants.CLEAR_USER,
};

const saveLocalStorageUser = (user: User) => {
  localStorage.setItem("authUser", JSON.stringify(user));
};

const getChatUID = async (serverUID: string) => {
  const server = (await db
    .collection("servers")
    .doc(serverUID)
    .get()
    .then((doc) => doc.data())) as Server;
  return server.defaultChat;
};

const signIn = throttle(
  async (dispatch: AppDispatch, email: string, password: string) =>
    new Promise<void>(async (_, reject) => {
      try {
        const { user } = await auth.signInWithEmailAndPassword(email, password);
        if (user) {
          dispatch(showLoader);

          const doc = await db.collection("users").doc(user.uid).get();
          const userDB = doc.data() as User;
          dispatch(setUser(userDB));
          saveLocalStorageUser(userDB);
        } else {
          reject({
            code: "something-wrong",
            message: "Что-то пошло не так",
          });
        }
      } catch (err) {
        reject(err);
      }
    }),
  1000
);

const signInThrottled =
  (
    email: string,
    password: string
  ): ThunkAction<Promise<void>, RootState, unknown, AnyAction> =>
  async (dispatch) =>
    await signIn(dispatch, email, password);

const signUp = throttle(
  async (
    dispatch: AppDispatch,
    name: string,
    email: string,
    password: string
  ) =>
    new Promise<void>(async (_, reject) => {
      try {
        const { user } = await auth.createUserWithEmailAndPassword(
          email,
          password
        );

        if (user) {
          dispatch(showLoader);

          const userInfo: User = {
            uid: user.uid,
            photoURL: "",
            displayName: name,
            servers: [],
          };

          db.collection("users").doc(user.uid).set(userInfo);
          dispatch(setUser(userInfo));
          saveLocalStorageUser(userInfo);
        } else {
          reject({
            code: "something-wrong",
            message: "Что-то пошло не так",
          });
        }
      } catch (err) {
        return reject(err);
      }
    }),
  500
);

const signUpThrottled =
  (
    name: string,
    email: string,
    password: string
  ): ThunkAction<Promise<void>, RootState, unknown, AnyAction> =>
  async (dispatch) =>
    await signUp(dispatch, name, email, password);

const signOut = throttle(async (dispatch: AppDispatch) => {
  try {
    await auth.signOut();
    dispatch(clearUser);
    localStorage.removeItem("authUser");
  } catch (err) {
    throw err;
  }
}, 1000);

const signOutThrottled =
  (): ThunkAction<void, RootState, unknown, AnyAction> => (dispatch) =>
    signOut(dispatch);

const checkServerExist = throttle(
  async (serverUID: string): Promise<boolean> => {
    const serverRef = db.collection("servers").doc(serverUID);

    const doc = await serverRef.get();

    return doc.exists;
  },
  1000
);

const checkServerExistThrottled = async (serverUID: string): Promise<boolean> =>
  checkServerExist(serverUID);

const inviteUserToServer = throttle(
  async (dispatch: AppThunkDispatch, user: User, serverUID: string) => {
    const newServers = [...user.servers, serverUID];

    await db.collection("users").doc(user.uid).update({ servers: newServers });

    const updatedUser = (await db
      .collection("users")
      .doc(user.uid)
      .get()
      .then((doc) => doc.data())) as User;

    dispatch(setUser(updatedUser));
    dispatch(initialServers(updatedUser.servers));
    saveLocalStorageUser(updatedUser);

    db.collection("servers")
      .doc(serverUID)
      .collection("members")
      .doc(user.uid)
      .set({
        uid: user.uid,
        displayName: user.displayName,
        photoURL: user.photoURL,
        status: "offline",
      });

    const chatUID = await getChatUID(serverUID);

    return chatUID;
  },
  1000
);

const inviteUserToServerThrottled =
  (
    user: User,
    serverUID: string
  ): ThunkAction<Promise<string>, RootState, unknown, AnyAction> =>
  async (dispatch) =>
    await inviteUserToServer(dispatch, user, serverUID);

export {
  signInThrottled,
  signUpThrottled,
  signOutThrottled,
  getChatUID,
  checkServerExistThrottled,
  inviteUserToServerThrottled,
};
