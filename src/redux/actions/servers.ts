import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { db } from "../../api";
import { RootState } from "../store";
import { Constants, ServersActionType, Server } from "../types";

const setServers = (servers: Server[]): ServersActionType => ({
  type: Constants.SET_SERVERS,
  payload: servers,
});

const setCurrentServer = (server: Server): ServersActionType => ({
  type: Constants.SET_CURRENT_SERVER,
  payload: server,
});

const initialCurrentServer = (
  serverUID: string
): ThunkAction<void, RootState, unknown, AnyAction> => async (dispatch) => {
  try {
    const doc = await db.collection("servers").doc(serverUID).get();

    dispatch(setCurrentServer({ uid: doc.id, ...doc.data() } as Server));
  } catch (err) {
    console.warn(err);
  }
};

const initialServers = (
  servers: string[]
): ThunkAction<void, RootState, unknown, AnyAction> => (dispatch) => {
  const serversRequests = servers.map((server: string) =>
    db.collection("servers").doc(server).get()
  );

  Promise.all(serversRequests).then((docs) => {
    const servers = docs.map((doc) => ({ uid: doc.id, ...doc.data() }));
    dispatch(setServers(servers as Server[]));
  });
};

export { initialServers, initialCurrentServer };
