import { Constants, ModalsActionType } from "../types";

const showChannelModal: ModalsActionType = {
  type: Constants.SHOW_CHANNEL_MODAL,
};

const closeChannelModal: ModalsActionType = {
  type: Constants.CLOSE_CHANNEL_MODAL,
};

const showServerModal: ModalsActionType = {
  type: Constants.SHOW_SERVER_MODAL,
};

const closeServerModal: ModalsActionType = {
  type: Constants.CLOSE_SERVER_MODAL,
};

const showLoader: ModalsActionType = {
  type: Constants.SHOW_LOADER,
};

const hideLoader: ModalsActionType = {
  type: Constants.HIDE_LOADER,
};

const showVoiceModal: ModalsActionType = {
  type: Constants.SHOW_VOICE_MODAL,
};

const closeVoiceModal: ModalsActionType = {
  type: Constants.CLOSE_VOICE_MODAL,
};

export {
  showChannelModal,
  closeChannelModal,
  showServerModal,
  closeServerModal,
  showLoader,
  hideLoader,
  showVoiceModal,
  closeVoiceModal,
};
