import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { RootState } from "../store";
import { Constants, PeersActionType } from "../types";

const setConnectionInfo = (
  serverUID: string,
  voiceUID: string
): ThunkAction<void, RootState, unknown, AnyAction> => (dispatch, getState) => {
  const { voices } = getState().voices;
  const { servers } = getState().servers;

  const connectionVoice = voices.filter(({ uid }) => uid === voiceUID)[0];
  const connectionServer = servers.filter(({ uid }) => uid === serverUID)[0];

  dispatch({
    type: Constants.SET_CONNECTION_INFO,
    payload: {
      connectionServer,
      connectionVoice,
    },
  });
};

const setTalkingStatus = (status: boolean): PeersActionType => ({
  type: Constants.SET_TALKING_STATUS,
  payload: status,
});

const clearConnectionInfo: PeersActionType = {
  type: Constants.CLEAR_CONNECTION_INFO,
};

export { setConnectionInfo, setTalkingStatus, clearConnectionInfo };
