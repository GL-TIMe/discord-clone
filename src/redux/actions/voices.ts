import { AnyAction } from "redux";

import { db } from "../../api";

import { RootState } from "../store";
import { ThunkAction } from "redux-thunk";
import { Constants, Voice, VoicesActionType } from "../types";

const setVoices = (voices: Voice[]): VoicesActionType => ({
  type: Constants.SET_VOICES,
  payload: voices,
});

const subscribeVoices = (
  serverUID: string
): ThunkAction<() => void, RootState, unknown, AnyAction> => (dispatch) => {
  const unsubscribe = db
    .collection("servers")
    .doc(serverUID)
    .collection("voices")
    .orderBy("createdAt")
    .onSnapshot((snapshot) => {
      dispatch(
        setVoices(
          snapshot.docs.map((doc) => ({
            uid: doc.id,
            ...doc.data(),
          })) as Voice[]
        )
      );
    });
  return unsubscribe;
};

export { subscribeVoices };
