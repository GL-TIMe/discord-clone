import { AnyAction } from "redux";
import { ThunkAction } from "redux-thunk";
import { db } from "../../api";
import { RootState } from "../store";
import { Constants, Member, MembersActionType } from "../types";

const setMembers = (members: Member[]): MembersActionType => ({
  type: Constants.SET_MEMBERS,
  payload: members,
});

const toggleMembers: MembersActionType = {
  type: Constants.TOGGLE_SHOW_MEMBERS,
};

const initialMembers = (
  serverUID: string
): ThunkAction<() => void, RootState, unknown, AnyAction> => (dispatch) => {
  const unsubscribe = db
    .collection("servers")
    .doc(serverUID)
    .collection("members")
    .onSnapshot((snapshot) =>
      dispatch(setMembers(snapshot.docs.map((doc) => doc.data()) as Member[]))
    );

  return unsubscribe;
};

export { initialMembers, toggleMembers };
