import { Chat, Constants, Message, MessagesActionType } from "../types";

type MessagesState = {
  currentChat: Chat;
  messages: Message[];
};

const initialState = {
  currentChat: Object.assign({}),
  messages: [],
} as MessagesState;

const messages = (
  state = initialState,
  action: MessagesActionType
): MessagesState => {
  switch (action.type) {
    case Constants.SET_MESSAGES:
      return {
        ...state,
        messages: action.payload,
      };
    default:
      return { ...state };
  }
};

export default messages;
