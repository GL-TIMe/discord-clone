import { Constants, User, UserActionType } from "../types";

export type UserState = {
  user: User;
};

const initUser = localStorage.getItem("authUser");

const initialState = {
  user: typeof initUser === "string" ? JSON.parse(initUser) : {},
} as UserState;

const user = (state = initialState, action: UserActionType) => {
  switch (action.type) {
    case Constants.SET_USER: {
      return {
        ...state,
        user: action.payload,
      };
    }
    case Constants.CLEAR_USER: {
      return {
        ...state,
        user: {} as User,
      };
    }
    default:
      return {
        ...state,
      };
  }
};

export default user;
