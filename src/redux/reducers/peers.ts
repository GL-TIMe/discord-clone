import { Constants, PeersActionType } from "../types";

type PeersStateType = {
  isTalking: boolean;
  connectionServerTitle: string;
  onnectionVoiceTitle: string;
  connectionServer: string;
  connectionVoice: string;
};

const initialState = {
  isTalking: false,
  connectionServerTitle: "",
  onnectionVoiceTitle: "",
  connectionServer: "",
  connectionVoice: "",
} as PeersStateType;

const peers = (state = initialState, action: PeersActionType) => {
  switch (action.type) {
    case Constants.SET_TALKING_STATUS: {
      return {
        ...state,
        isTalking: action.payload,
      };
    }
    case Constants.SET_CONNECTION_INFO: {
      return {
        ...state,
        connectionServer: action.payload.connectionServer.uid,
        connectionServerTitle: action.payload.connectionServer.title,
        connectionVoice: action.payload.connectionVoice.uid,
        onnectionVoiceTitle: action.payload.connectionVoice.title,
      };
    }
    case Constants.CLEAR_CONNECTION_INFO: {
      return {
        ...state,
        connectionServerTitle: "",
        onnectionVoiceTitle: "",
        connectionServer: "",
        connectionVoice: "",
      };
    }
    default: {
      return {
        ...state,
      };
    }
  }
};

export default peers;
