import { Constants, ModalsActionType } from "../types";

type ModalsState = {
  channel: boolean;
  server: boolean;
  loader: boolean;
  voice: boolean;
};

const initialState = {
  channel: false,
  server: false,
  loader: false,
  voice: false,
} as ModalsState;

const modals = (state = initialState, action: ModalsActionType) => {
  switch (action.type) {
    case Constants.SHOW_CHANNEL_MODAL:
      return {
        ...state,
        channel: true,
      };
    case Constants.CLOSE_CHANNEL_MODAL:
      return {
        ...state,
        channel: false,
      };
    case Constants.SHOW_SERVER_MODAL:
      return {
        ...state,
        server: true,
      };
    case Constants.CLOSE_SERVER_MODAL:
      return {
        ...state,
        server: false,
      };
    case Constants.SHOW_LOADER:
      return {
        ...state,
        loader: true,
      };
    case Constants.HIDE_LOADER:
      return {
        ...state,
        loader: false,
      };
    case Constants.SHOW_VOICE_MODAL:
      return {
        ...state,
        voice: true,
      };
    case Constants.CLOSE_VOICE_MODAL:
      return {
        ...state,
        voice: false,
      };
    default:
      return { ...state };
  }
};

export default modals;
