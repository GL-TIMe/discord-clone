import { Constants, Member, MembersActionType } from "../types";

type MembersState = {
  members: Member[];
  show: boolean;
};

const initialState = {
  members: [],
  show: true,
} as MembersState;

const members = (state = initialState, action: MembersActionType) => {
  switch (action.type) {
    case Constants.SET_MEMBERS:
      return {
        ...state,
        members: action.payload,
      };
    case Constants.TOGGLE_SHOW_MEMBERS:
      return {
        ...state,
        show: !state.show,
      };
    default:
      return {
        ...state,
      };
  }
};

export default members;
