import { combineReducers } from "redux";

import messages from "./messages";
import chats from "./chats";
import voices from "./voices";
import user from "./user";
import servers from "./servers";
import members from "./members";
import modals from "./modals";
import peers from "./peers";

const root = combineReducers({
  messages,
  chats,
  voices,
  user,
  servers,
  modals,
  members,
  peers,
});

export default root;
