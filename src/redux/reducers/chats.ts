import { Chat, ChatsActionType, Constants } from "../types";

type ChatState = {
  chats: Chat[];
  currentChat: Chat;
};

const initialState = {
  chats: [],
  currentChat: Object.assign({}),
} as ChatState;

const chats = (state = initialState, action: ChatsActionType) => {
  switch (action.type) {
    case Constants.SET_CHATS:
      return {
        ...state,
        chats: action.payload,
      };
    case Constants.SET_CURRENT_CHAT:
      return {
        ...state,
        currentChat: action.payload,
      };
    default:
      return { ...state };
  }
};

export default chats;
