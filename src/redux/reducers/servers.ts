import { Constants, Server, ServersActionType } from "../types";

type ServersState = {
  servers: Server[];
  currentServer: Server;
};

const initialState = {
  servers: [],
  currentServer: Object.assign({}),
} as ServersState;

const servers = (state = initialState, action: ServersActionType) => {
  switch (action.type) {
    case Constants.SET_SERVERS:
      return {
        ...state,
        servers: action.payload,
      };
    case Constants.SET_CURRENT_SERVER:
      return {
        ...state,
        currentServer: action.payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default servers;
