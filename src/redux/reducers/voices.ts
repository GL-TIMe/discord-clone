import { Constants, Voice, VoicesActionType } from "../types";

type VoicesState = {
  voices: Voice[];
};

const initialState = {
  voices: [],
} as VoicesState;

const voices = (state = initialState, action: VoicesActionType) => {
  switch (action.type) {
    case Constants.SET_VOICES:
      return {
        ...state,
        voices: action.payload,
      };
    default:
      return { ...state };
  }
};

export default voices;
