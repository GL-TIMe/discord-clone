const SWITCH_SPACE_TO_DASH_REGEXP = new RegExp(" ", "g");
const DELETE_MULTIDASH_REGEXP = new RegExp("--+", "g");
const DELETE_NOT_ALLOWED_SYMBOLS_REGEXP = new RegExp("[^а-яёa-z0-9_-]", "g");

const validateChannelName = (text: string) => {
  let modifiedText = text
    .toLowerCase()
    .slice(0, 64)
    .replace(SWITCH_SPACE_TO_DASH_REGEXP, "-")
    .replace(DELETE_MULTIDASH_REGEXP, "-")
    .replace(DELETE_NOT_ALLOWED_SYMBOLS_REGEXP, "");

  return modifiedText;
};

export { validateChannelName };
