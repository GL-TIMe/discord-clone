import React, { useState } from "react";

type ValidationsStateTypes = {
  isEmpty: boolean;
  isPassword: boolean;
  isUsername: boolean;
};

type ValidationsTypes = {
  [validation in keyof ValidationsStateTypes]?: boolean;
};

enum ErrorsMessages {
  IS_EMPTY = "Обязательное поле",
  IS_PASSWORD = "Пароль должен содержать: минимум 8 символов, как минимум одну заглавную букву, одну строчную букву и одну цифру",
  IS_USERNAME = "Имя пользователя должно содержать от 8 до 20 символов, недолжно начинаться и заканчиваться на _ или ., не может иметь больше одного _ или . подряд",
}

const getErrorMessage = ({
  isEmpty,
  isPassword,
  isUsername,
}: ValidationsStateTypes) => {
  if (isEmpty) {
    return ErrorsMessages.IS_EMPTY;
  }
  if (isUsername) {
    return ErrorsMessages.IS_USERNAME;
  }
  if (isPassword) {
    return ErrorsMessages.IS_PASSWORD;
  }

  return "";
};

const PASSWORD_REGEXP = new RegExp(
  "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9@$!%*?&]{8,}$"
);
const USERNAME_REGEXP = new RegExp(
  "^(?=[a-zA-Z0-9._]{8,20}$)(?!.*[_.]{2})[^_.].*[^_.]$"
);

type ObjectKeys<T> = T extends object
  ? (keyof T)[]
  : T extends number
  ? []
  : T extends Array<any> | string
  ? string[]
  : never;

declare global {
  interface ObjectConstructor {
    keys<T>(o: T): ObjectKeys<T>;
  }
}

const doValidation = (value: string, validations: ValidationsTypes) => {
  let isEmpty = false;
  let isPassword = false;
  let isUsername = false;
  let inputInvalid = false;

  for (const validation of Object.keys(validations)) {
    if (validations[validation]) {
      switch (validation) {
        case "isPassword":
          isPassword = !PASSWORD_REGEXP.test(value);
          break;
        case "isUsername":
          isUsername = !USERNAME_REGEXP.test(value);
          break;
        case "isEmpty":
          isEmpty = !value;
          break;
      }
    }
  }

  inputInvalid = isEmpty || isPassword || isUsername;

  return {
    isEmpty,
    isPassword,
    isUsername,
    inputInvalid,
  };
};

const useInput = (initialValue: string, validations: ValidationsTypes) => {
  const [value, setValue] = useState<string>(initialValue);
  const [errorMessage, setErrorMessage] = useState<string>("");
  const [isDirty, setDirty] = useState<boolean>(false);

  const onSubmit = () => {
    const valid = doValidation(value, validations);

    if (valid.inputInvalid) {
      setDirty(true);
      setErrorMessage(getErrorMessage(valid));
    } else {
      setDirty(false);
      setErrorMessage("");
    }
    return valid.inputInvalid;
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  return {
    value,
    onChange,
    errorMessage,
    setErrorMessage,
    isDirty,
    setDirty,
    onSubmit,
  };
};

export { useInput };
