import React from "react";
import Servers from "./servers/";
import Login from "./login";
import Register from "./register";
import Invite from "./invite";
import { ServersNotFound } from "./servers/components/ServersNotFound";

enum Routes {
  LOGIN_ROUTE = "/login",
  REGISTER_ROUTE = "/register",
  SERVERS_AND_CHAT_ROUTE = "/channels/:serverUID/:chatIUD",
  INVITE_ROUTE = "/invite/:serverUID",
  SERVERS_NOT_FOUND_ROUTE = "/servers-not-found",
}

type RouteType = {
  path: string;
  Component: React.ComponentType<any>;
};

const publicRoutes: RouteType[] = [
  {
    path: Routes.LOGIN_ROUTE,
    Component: Login,
  },
  {
    path: Routes.REGISTER_ROUTE,
    Component: Register,
  },
];

const privateRoutes: RouteType[] = [
  {
    path: Routes.SERVERS_AND_CHAT_ROUTE,
    Component: Servers,
  },
  {
    path: Routes.SERVERS_NOT_FOUND_ROUTE,
    Component: ServersNotFound,
  },
];

const commonRoutes: RouteType[] = [
  {
    path: Routes.INVITE_ROUTE,
    Component: Invite,
  },
];

export { Routes, publicRoutes, privateRoutes, commonRoutes };
