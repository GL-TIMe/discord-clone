import React from "react";
import { Link } from "react-router-dom";

import { LoginForm } from "../components/LoginForm";

const Login: React.FC = () => {
  return (
    <nav className="auth__container">
      <div className="auth__box">
        <header className="auth__header">
          <h1 className="auth__title">С возвращением!</h1>
          <h2 className="auth__subtitle">Мы так рады видеть вас снова!</h2>
        </header>

        <LoginForm />
        <footer className="auth__footer">
          <p className="auth__text">
            Нужна учётная запись?{" "}
            <Link className="auth__link" to="/register">
              Зарегистрироваться
            </Link>
          </p>
        </footer>
      </div>
    </nav>
  );
};

export default Login;
