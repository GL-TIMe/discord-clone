import React, { useEffect, useRef } from "react";
import { format } from "date-fns";
import { subscribeMessages } from "../../redux/actions/messages";
import { useAppSelector, useThunkDispatch } from "../../redux/hook";
import { Message } from "../../redux/types";
import { shallowEqual } from "react-redux";
import { TextChannelIcon } from "../../icons";

type GreetingProps = {
  title: string;
};

const DefaultGreeting: React.FC<GreetingProps> = ({ title }) => (
  <div className="messages__greeting">
    <div className="messages__greeting-icon">
      <TextChannelIcon width={44} height={44} />
    </div>
    <div>
      <h2 className="messages__title">Добро пожаловать на канал #{title}!</h2>
    </div>
    <div className="messages__subtitle">Это начало канала #{title}.</div>
  </div>
);

const EmptyGreeting: React.FC<GreetingProps> = ({ title }) => (
  <div className="messages__greeting empty">
    <div>
      <h2 className="messages__title">Добро пожаловать на канал #{title}</h2>
    </div>
    <div className="messages__subtitle">Ваше сообщение может стать первым</div>
  </div>
);

type MessageProps = {
  serverUID: string;
  chatUID: string;
};

const Messages: React.FC<MessageProps> = ({ serverUID, chatUID }) => {
  const messagesRef = useRef<HTMLDivElement>(null);

  const dispatch = useThunkDispatch();
  const { messages } = useAppSelector((state) => state.messages, shallowEqual);
  const { title } = useAppSelector(
    (state) => state.chats.currentChat,
    shallowEqual
  );

  useEffect(() => {
    if (messagesRef.current) {
      messagesRef.current.scrollTop = messagesRef.current.scrollHeight;
    }
  }, [messages]);

  useEffect(() => {
    const unsubscribe = dispatch(subscribeMessages(serverUID, chatUID));

    return unsubscribe;
  }, [dispatch, serverUID, chatUID]);

  return (
    <section ref={messagesRef} className="messages">
      <div className="messages__items">
        {messages.length ? (
          <DefaultGreeting title={title} />
        ) : (
          <EmptyGreeting title={title} />
        )}

        {messages.map(
          ({ uid, createdAt: { seconds }, displayName, text }: Message) => {
            const time = format(new Date(seconds * 1000), "HH:mm");
            return (
              <div className="messages__item" key={uid}>
                <h2 className="messages__info">
                  <span className="messages__time">{time}</span>
                  <span className="messages__author">{displayName}</span>
                </h2>
                <div className="messages__text">{text}</div>
              </div>
            );
          }
        )}
      </div>
    </section>
  );
};

export { Messages };
