import React, { useEffect } from "react";
import { connect } from "react-redux";

import { initialMembers } from "../../redux/actions/members";
import { AppThunkDispatch, RootState } from "../../redux/store";

import defaultAvatarImg from "../../img/default-avatar.png";

type MembersSideBarProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & { serverUID: string };

const MembersSideBar: React.FC<MembersSideBarProps> = ({
  members,
  show,
  initialMebers,
  serverUID,
}) => {
  useEffect(() => {
    const unsubscribe = initialMebers(serverUID);

    return unsubscribe;
  }, [initialMebers, serverUID]);

  const onlineMembers = members.filter(({ status }) => status === "online");
  const offlineMembers = members.filter(({ status }) => status === "offline");

  return show ? (
    <aside className="members">
      {onlineMembers.length ? (
        <div className="members__online">
          <h2 className="members__title">В сети - {onlineMembers.length}</h2>

          {onlineMembers.map(({ displayName, uid, photoURL }) => (
            <div key={uid} className="members__item person">
              <div className="person__avatar">
                <img
                  className="person__img"
                  src={photoURL ? photoURL : defaultAvatarImg}
                  alt="member avatar"
                />
              </div>

              <div>
                <h2 className="person__name members__name">{displayName}</h2>
              </div>
            </div>
          ))}
        </div>
      ) : null}

      {offlineMembers.length ? (
        <div className="members__offline">
          <h2 className="members__title">
            Не в сети - {offlineMembers.length}
          </h2>

          {offlineMembers.map(({ displayName, uid, photoURL }) => (
            <div
              key={uid}
              className="person members__item members__item_offline"
            >
              <div className="person__avatar">
                <img
                  className="person__img"
                  src={photoURL ? photoURL : defaultAvatarImg}
                  alt="member avatar"
                />
              </div>

              <div>
                <h2 className="person__name members__name">{displayName}</h2>
              </div>
            </div>
          ))}
        </div>
      ) : null}
    </aside>
  ) : null;
};

const mapStateToProps = (state: RootState) => ({
  members: state.members.members,
  show: state.members.show,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  initialMebers: (serverUID: string) => dispatch(initialMembers(serverUID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MembersSideBar);
