import React, { useEffect } from "react";
import { shallowEqual } from "react-redux";
import { useHistory } from "react-router";
import { throttle } from "../../helpers/throttle";
import { getChatUID } from "../../redux/actions/user";
import { useAppSelector } from "../../redux/hook";

const ChannelsRedirect: React.FC = () => {
  const history = useHistory();
  const { user } = useAppSelector((state) => state.user, shallowEqual);
  const serverUID = user.servers[0];

  useEffect(() => {
    throttle.cancel();
    if (serverUID) {
      getChatUID(serverUID).then((chatUID) =>
        history.push(`/channels/${serverUID}/${chatUID}`)
      );
    } else {
      history.push(`/servers-not-found`);
    }
  }, [serverUID, history]);

  return null;
};

export { ChannelsRedirect };
