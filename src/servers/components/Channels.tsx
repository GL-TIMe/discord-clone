import React from "react";

import { RouteComponentProps, withRouter } from "react-router-dom";

import ChannelsSideBar from "./ChannelsSideBar";
import Chat from "./Chat";

type MatchParams = {
  serverUID: string;
  chatUID: string;
};

interface MatchProps extends RouteComponentProps<MatchParams> {}

type ChannelsTypes = MatchProps & { serverUID: string };

const Channels: React.FC<ChannelsTypes> = ({ serverUID, match }) => {
  const { chatUID } = match.params;

  return chatUID ? (
    <>
      <ChannelsSideBar serverUID={serverUID} />
      <Chat serverUID={serverUID} chatUID={chatUID} />
    </>
  ) : null;
};

export default withRouter(Channels);
