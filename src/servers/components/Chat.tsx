import React, { useEffect } from "react";
import { connect } from "react-redux";
import { AppThunkDispatch, RootState } from "../../redux/store";

import ChannelHeader from "./ChannelHeader";
import MembersSideBar from "./MembersSideBar";
import { Messages } from "./Messages";
import { TextareaField } from "./TextareaField";

import { addMessage } from "../../api";
import { initialCurrentChat } from "../../redux/actions/chats";

type ChatProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & {
    serverUID: string;
    chatUID: string;
  };

const Chat: React.FC<ChatProps> = ({
  serverUID,
  chatUID,
  currentChat,
  user,
  initialCurrentChat,
}) => {
  useEffect(() => {
    initialCurrentChat(serverUID, chatUID);
  }, [initialCurrentChat, serverUID, chatUID]);

  const { title } = currentChat;

  const onSubmit = (text: string) => addMessage(user, serverUID, chatUID, text);

  return (
    <div className="home__container">
      <ChannelHeader title={title} />
      <div className="home__content">
        <main className="home__messages">
          <Messages serverUID={serverUID} chatUID={chatUID} />
          <TextareaField onSubmit={onSubmit} placeholder={title} />
        </main>
        <MembersSideBar serverUID={serverUID} />
      </div>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
  currentChat: state.chats.currentChat,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  initialCurrentChat: (serverUID: string, chatUID: string) =>
    dispatch(initialCurrentChat(serverUID, chatUID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
