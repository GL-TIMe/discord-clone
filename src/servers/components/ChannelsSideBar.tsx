import React from "react";

import { connect } from "react-redux";
import { RootState } from "../../redux/store";

import ChatsList from "./ChatsList";
import User from "./User";
import VoicesList from "./VoicesList";

type ChannelsSideBarProps = ReturnType<typeof mapStateToProps> & {
  serverUID: string;
};

const ChannelsSideBar: React.FC<ChannelsSideBarProps> = ({
  currentServer,
  serverUID,
}) => {
  const { title: serverName } = currentServer;

  return (
    <nav className="channels">
      <header className="channels__header">
        <h2 className="channels__title">{serverName}</h2>
      </header>
      <div className="channels__wrapper">
        <div className="channels__items">
          <ChatsList serverUID={serverUID} />
          <VoicesList serverUID={serverUID} />
        </div>
      </div>

      <User />
    </nav>
  );
};

const mapStateToProps = (state: RootState) => ({
  currentServer: state.servers.currentServer,
});

export default connect(mapStateToProps)(ChannelsSideBar);
