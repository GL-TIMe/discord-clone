import React from "react";
import { connect } from "react-redux";
import { PutDownIcon, SignoutIcon } from "../../icons";
import { signOutThrottled } from "../../redux/actions/user";
import { AppThunkDispatch, RootState } from "../../redux/store";

import defaultAvatar from "../../img/default-avatar.png";
import socket from "../../socket";
import { leaveFromVoice } from "../../api";
import {
  clearConnectionInfo,
  setTalkingStatus,
} from "../../redux/actions/peers";

type UserProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps>;

const User: React.FC<UserProps> = ({
  user,
  signOutThrottled,
  isTalking,
  connectionServer,
  connectionVoice,
  clearConnectionInfo,
  connectionServerTitle,
  connectionVoiceTitle,
  setTalkingStatus,
}) => {
  const { displayName, photoURL, uid: userUID } = user;

  const handleSignOut = () => {
    signOutThrottled();
  };

  const handlePutDown = () => {
    if (!connectionVoice) {
      return;
    }

    leaveFromVoice(connectionServer, connectionVoice, userUID);
    window.localStream.getTracks().forEach((track) => track.stop());
    window.peersRef.forEach(({ peer }) => peer.destroy());
    socket.removeAllListeners();
    socket.emit("leave");
    setTalkingStatus(false);
    clearConnectionInfo();
  };

  return (
    <section className="user">
      {isTalking ? (
        <div className="user__voice">
          <div className="user__connect">
            <div className="user__connect-status">
              Голосовая связь подключена
            </div>
            <div className="user__connect-info">
              {connectionVoiceTitle} / {connectionServerTitle}
            </div>
          </div>

          <div className="user__tools">
            <button className="user__tool" onClick={handlePutDown}>
              <PutDownIcon className="user__icon" width={20} height={20} />
            </button>
          </div>
        </div>
      ) : null}

      <div className="user__wrapper person">
        <div className="person__avatar">
          <img
            className="person__img"
            src={photoURL ? photoURL : defaultAvatar}
            alt="user avatar"
          />
        </div>

        <h2 className="person__name">{displayName}</h2>

        <div className="user__tools">
          <button onClick={handleSignOut} className="user__tool">
            <SignoutIcon className="user__icon" width={20} height={20} />
          </button>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
  connectionServer: state.peers.connectionServer,
  connectionServerTitle: state.peers.connectionServerTitle,
  connectionVoice: state.peers.connectionVoice,
  connectionVoiceTitle: state.peers.onnectionVoiceTitle,
  isTalking: state.peers.isTalking,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  signOutThrottled: () => dispatch(signOutThrottled()),
  clearConnectionInfo: () => dispatch(clearConnectionInfo),
  setTalkingStatus: (status: boolean) => dispatch(setTalkingStatus(status)),
});

export default connect(mapStateToProps, mapDispatchToProps)(User);
