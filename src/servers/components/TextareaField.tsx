import React, { useState } from "react";

type TextareaFieldProps = {
  onSubmit: (text: string) => void;
  placeholder: string;
};

const MIN_ROWS = 1;
const MAX_ROWS = 10;
const TEXTAREA_FIELD_HEIGHT = 24.1;

const TextareaField: React.FC<TextareaFieldProps> = ({
  onSubmit,
  placeholder,
}) => {
  const [value, setValue] = useState<string>("");
  const [rows, setRows] = useState<number>(1);

  const handleChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const previousRows = e.target.rows;
    e.target.rows = MIN_ROWS;

    const currentRows = ~~(e.target.scrollHeight / TEXTAREA_FIELD_HEIGHT);

    if (currentRows === previousRows) {
      e.target.rows = currentRows;
    }

    if (currentRows >= MAX_ROWS) {
      e.target.rows = MAX_ROWS;
      e.target.scrollTop = e.target.scrollHeight;
    }

    setRows(currentRows < MAX_ROWS ? currentRows : MAX_ROWS);
    setValue(e.target.value);
  };

  const onEnterPress = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
    if (e.key === "Enter" && e.shiftKey === false) {
      e.preventDefault();

      const clearText = value.trim();

      if (clearText.length) {
        onSubmit(clearText);
        setValue("");
        setRows(1);
      }
    }
  };

  return (
    <div className="text-field__wrapper">
      <textarea
        className="text-field__textarea"
        placeholder={`Написать в #${placeholder}`}
        name="message"
        rows={rows}
        value={value}
        onChange={handleChange}
        onKeyDown={onEnterPress}
      ></textarea>
    </div>
  );
};

export { TextareaField };
