import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hook";

import { Button } from "../../components/Button";
import { InputField } from "../../components/InputField";
import { CloseButton } from "../../icons";
import { validateChannelName } from "../../helpers/modal-validate";
import { closeVoiceModal } from "../../redux/actions/modals";
import { addVoiceThrottled } from "../../api";

type AddChannelModalProps = {
  serverUID: string;
};

const AddVoiceModal: React.FC<AddChannelModalProps> = ({ serverUID }) => {
  const [disabled, setDisabled] = useState<boolean>(true);
  const [name, setName] = useState<string>("");

  const { voice } = useAppSelector((state) => state.modals);

  const dispatch = useAppDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = validateChannelName(e.target.value);
    setName(value);
    if (value) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (name) {
      await addVoiceThrottled(serverUID, name);
      handleClose();
    }
  };

  const handleClose = () => {
    dispatch(closeVoiceModal);
    setName("");
    setDisabled(true);
  };

  return voice ? (
    <section className="modal">
      <form className="modal__container" onSubmit={handleSubmit}>
        <header className="modal__header">
          <h2 className="modal__title">Создать голосовой канал</h2>
          <button className="modal__close" type="button" onClick={handleClose}>
            <CloseButton className="modal__close-icon" width={24} height={24} />
          </button>
        </header>

        <main className="modal__content">
          <InputField
            type="text"
            label="Название канала"
            value={name}
            onChange={handleChange}
          />
        </main>

        <footer className="modal__footer">
          <Button
            className="modal__button modal__button_outline"
            onClick={handleClose}
          >
            <div className="modal__button-content">Отмена</div>
          </Button>
          <Button className="modal__button" type="submit" disabled={disabled}>
            <div className="modal__button-content">Создать канал</div>
          </Button>
        </footer>
      </form>
    </section>
  ) : null;
};

export { AddVoiceModal };
