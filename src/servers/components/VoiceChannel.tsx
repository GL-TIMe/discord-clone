import React, { useEffect, useState } from "react";

import { VoiceChannelIcon } from "../../icons";
import { Voice } from "../../redux/types";
import { db, leaveFromVoice, updateVoiceMembers } from "../../api";

import defaultAvatar from "../../img/default-avatar.png";
import socket from "../../socket";
import {
  clearConnectionInfo,
  setConnectionInfo,
  setTalkingStatus,
} from "../../redux/actions/peers";
import { connect } from "react-redux";
import { AppDispatch, AppThunkDispatch, RootState } from "../../redux/store";

type VoiceChannelProps = {
  voice: Voice;
  serverUID: string;
} & ReturnType<typeof mapDispatchToProps> &
  ReturnType<typeof mapStateToProps>;

type VoiceMember = {
  photoURL: string;
  displayName: string;
  uid: string;
};

const VoiceChannel: React.FC<VoiceChannelProps> = ({
  voice,
  serverUID,
  user,
  connectionVoice,
  connectionServer,
  setConnectionInfo,
  setTalkingStatus,
  clearConnectionInfo,
}) => {
  const { title, uid: voiceUID } = voice;
  const { uid: userUID } = user;

  const [members, setMembers] = useState<VoiceMember[]>([]);

  const handleClick = () => {
    if (connectionVoice === voiceUID) {
      return;
    }

    if (connectionVoice) {
      leaveFromVoice(connectionServer, connectionVoice, userUID);
      window.localStream.getTracks().forEach((track) => track.stop());
      socket.emit("leave");
      socket.removeAllListeners();
      clearConnectionInfo();
      setTalkingStatus(false);
    }

    updateVoiceMembers(serverUID, voiceUID, user);
    setConnectionInfo(serverUID, voiceUID);
    setTalkingStatus(true);
  };

  useEffect(() => {
    const unsubcribe = db
      .collection("servers")
      .doc(serverUID)
      .collection("voices")
      .doc(voiceUID)
      .collection("members")
      .onSnapshot((snaphots) => {
        setMembers(
          snaphots.docs.map((doc) => ({
            uid: doc.id,
            ...doc.data(),
          })) as VoiceMember[]
        );
      });

    return unsubcribe;
  }, [serverUID, voiceUID]);

  return (
    <li onClick={handleClick}>
      <div className="channels__item">
        <VoiceChannelIcon className="channels__icon" width={20} height={20} />
        <h4 className="channels__name">{title}</h4>
      </div>
      {members.map(({ uid, displayName, photoURL }: any) => (
        <div className="channels__member" key={uid}>
          <div className="channels__member-avatar">
            <img
              className="channels__member-img"
              src={photoURL ? photoURL : defaultAvatar}
              alt={`${displayName} voice member`}
            />
          </div>
          <span className="channels__member-name">{displayName}</span>
        </div>
      ))}
    </li>
  );
};

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
  connectionVoice: state.peers.connectionVoice,
  connectionServer: state.peers.connectionServer,
});

const mapDispatchToProps = (dispatch: AppDispatch & AppThunkDispatch) => ({
  setConnectionInfo: (serverUID: string, voiceUID: string) =>
    dispatch(setConnectionInfo(serverUID, voiceUID)),
  setTalkingStatus: (status: boolean) => dispatch(setTalkingStatus(status)),
  clearConnectionInfo: () => dispatch(clearConnectionInfo),
});

export default connect(mapStateToProps, mapDispatchToProps)(VoiceChannel);
