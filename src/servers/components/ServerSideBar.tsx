import React from "react";
import { Link } from "react-router-dom";
import { Server } from "../../redux/types";
import clsx from "clsx";
import { AddServerButton } from "../../icons";
import { useAppDispatch } from "../../redux/hook";
import { showServerModal } from "../../redux/actions/modals";

type ServersSideBarProps = {
  servers: Server[];
  serverUID: string;
};

const ServersSideBar: React.FC<ServersSideBarProps> = ({
  serverUID,
  servers,
}) => {
  const dispatch = useAppDispatch();

  const handleAddServer = () => {
    dispatch(showServerModal);
  };

  return (
    <aside className="servers">
      <ul className="servers__items">
        {servers.map(({ uid, defaultChat, title, photoURL }) => (
          <li className="servers__item" title={title} key={uid}>
            <Link
              to={`/channels/${uid}/${defaultChat}`}
              className={clsx("servers__avatar", {
                selected: serverUID === uid,
              })}
            >
              <img className="servers__avatar-img" src={photoURL} alt={title} />
            </Link>
          </li>
        ))}
        <li className="servers__item" title="Добавить сервер">
          <div className="servers__add" onClick={handleAddServer}>
            <AddServerButton
              className="servers__add-icon"
              width={24}
              height={24}
            />
          </div>
        </li>
      </ul>
    </aside>
  );
};

export { ServersSideBar };
