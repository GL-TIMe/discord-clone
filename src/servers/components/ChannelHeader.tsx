import React from "react";
import { connect } from "react-redux";

import { toggleMembers } from "../../redux/actions/members";

import { TextChannelIcon, MembersIcon } from "../../icons";

import { AppDispatch } from "../../redux/store";

type ChannelHeaderProps = ReturnType<typeof mapDispatchToProps> & {
  title: string;
};

const ChannelHeader: React.FC<ChannelHeaderProps> = ({
  title,
  toggleMembers,
}) => {
  const handleShowMembers = () => {
    toggleMembers();
  };

  return (
    <section className="header">
      <div className="header__info">
        <TextChannelIcon className="header__icon" width={24} height={24} />
        <h3 className="header__title">{title}</h3>
      </div>
      <div>
        <button
          title="Список участников"
          className="header__tool"
          onClick={handleShowMembers}
        >
          <MembersIcon className="header__tool-icon" width={24} height={24} />
        </button>
      </div>
    </section>
  );
};

const mapDispatchToProps = (dispatch: AppDispatch) => ({
  toggleMembers: () => dispatch(toggleMembers),
});

export default connect(undefined, mapDispatchToProps)(ChannelHeader);
