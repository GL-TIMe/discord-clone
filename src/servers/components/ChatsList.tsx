import React, { useEffect } from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import { AddIcon, TextChannelIcon } from "../../icons";
import { subscribeChats } from "../../redux/actions/chats";
import { showChannelModal } from "../../redux/actions/modals";
import { AppDispatch, AppThunkDispatch, RootState } from "../../redux/store";

type ChatsListProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & {
    serverUID: string;
  };

const ChatsList: React.FC<ChatsListProps> = ({
  serverUID,
  chats,
  showChannelModal,
  subscribeChats,
}) => {
  const handleChannelAdd = () => {
    showChannelModal();
  };

  useEffect(() => {
    const unsubscribe = subscribeChats(serverUID);

    return unsubscribe;
  }, [serverUID, subscribeChats]);

  return (
    <div>
      <div className="channels__group">
        <h3 className="channels__group-name">Текстовые каналы</h3>
        <button className="channels__add" onClick={handleChannelAdd}>
          <AddIcon width={18} height={18} className="channels__add-icon" />
        </button>
      </div>

      <ul>
        {chats.map(({ title, uid }) => (
          <li key={uid}>
            <NavLink
              className="channels__item"
              to={`/channels/${serverUID}/${uid}`}
              activeClassName="selected"
            >
              <TextChannelIcon
                className={"channels__icon"}
                width={20}
                height={20}
              />
              <h4 className="channels__name">{title}</h4>
            </NavLink>
          </li>
        ))}
      </ul>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  chats: state.chats.chats,
});

const mapDispatchToProps = (dispatch: AppDispatch & AppThunkDispatch) => ({
  showChannelModal: () => dispatch(showChannelModal),
  subscribeChats: (serverUID: string) => dispatch(subscribeChats(serverUID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatsList);
