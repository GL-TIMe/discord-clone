import React, { useEffect } from "react";
import { hideLoader } from "../../redux/actions/modals";
import { useAppDispatch } from "../../redux/hook";
import { ServersSideBar } from "./ServerSideBar";
import User from "./User";

const ServersNotFound = () => {
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(hideLoader);
  }, [dispatch]);

  return (
    <div className="home">
      <ServersSideBar servers={[]} serverUID="" />
      <nav className="channels">
        <User />
      </nav>
      <div className="not-found-servers">
        <h2 className="not-found-servers__title">
          У вас нет доступных серверов, возможно вы впервые у нас, пожалуйста
          добавьте сервер, прежде чем продолжить.
        </h2>
      </div>
    </div>
  );
};

export { ServersNotFound };
