import React, { useState } from "react";
import { useAppDispatch, useAppSelector } from "../../redux/hook";

import { closeChannelModal } from "../../redux/actions/modals";

import { Button } from "../../components/Button";
import { InputField } from "../../components/InputField";
import { CloseButton } from "../../icons";
import { addChannelThrottled } from "../../api";
import { useHistory } from "react-router-dom";
import { validateChannelName } from "../../helpers/modal-validate";

type AddChannelModalProps = {
  serverUID: string;
};

const AddChannelModal: React.FC<AddChannelModalProps> = ({ serverUID }) => {
  const history = useHistory();
  const [disabled, setDisabled] = useState<boolean>(true);
  const [name, setName] = useState<string>("");

  const { channel } = useAppSelector((state) => state.modals);

  const dispatch = useAppDispatch();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    let value = validateChannelName(e.target.value);
    setName(value);
    if (value) {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  };

  const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (name) {
      const newChatUID = await addChannelThrottled(serverUID, name).then(
        (data) => data.id,
        (err) => ""
      );
      handleClose();
      history.push(`/channels/${serverUID}/${newChatUID}`);
    }
  };

  const handleClose = () => {
    dispatch(closeChannelModal);
    setName("");
    setDisabled(true);
  };

  return channel ? (
    <section className="modal">
      <form className="modal__container" onSubmit={handleSubmit}>
        <header className="modal__header">
          <h2 className="modal__title">Создать текстовый канал</h2>
          <button className="modal__close" type="button" onClick={handleClose}>
            <CloseButton className="modal__close-icon" width={24} height={24} />
          </button>
        </header>

        <main className="modal__content">
          <InputField
            type="text"
            label="Название канала"
            value={name}
            onChange={handleChange}
          />
        </main>

        <footer className="modal__footer">
          <Button
            className="modal__button modal__button_outline"
            onClick={handleClose}
          >
            <div className="modal__button-content">Отмена</div>
          </Button>
          <Button className="modal__button" type="submit" disabled={disabled}>
            <div className="modal__button-content">Создать канал</div>
          </Button>
        </footer>
      </form>
    </section>
  ) : null;
};

export { AddChannelModal };
