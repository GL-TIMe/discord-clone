import React, { useEffect } from "react";
import { connect } from "react-redux";
import { AddIcon } from "../../icons";
import { showVoiceModal } from "../../redux/actions/modals";

import { subscribeVoices } from "../../redux/actions/voices";
import { AppThunkDispatch, RootState } from "../../redux/store";

import VoiceChannel from "./VoiceChannel";

type VoicesListProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> & { serverUID: string };

const VoicesList: React.FC<VoicesListProps> = ({
  voices,
  serverUID,
  subscribeVoices,
  showVoiceModal,
}) => {
  useEffect(() => {
    const unsubscribe = subscribeVoices(serverUID);

    return unsubscribe;
  }, [subscribeVoices, serverUID]);

  const handleChannelAdd = () => {
    showVoiceModal();
  };

  return (
    <div>
      <div className="channels__group">
        <h3 className="channels__group-name">Голосовые каналы</h3>
        <button className="channels__add" onClick={handleChannelAdd}>
          <AddIcon width={18} height={18} className="channels__add-icon" />
        </button>
      </div>

      <ul>
        {voices.map((voice) => (
          <VoiceChannel key={voice.uid} voice={voice} serverUID={serverUID} />
        ))}
      </ul>
    </div>
  );
};

const mapStateToProps = (state: RootState) => ({
  voices: state.voices.voices,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  subscribeVoices: (serverUID: string) => dispatch(subscribeVoices(serverUID)),
  showVoiceModal: () => dispatch(showVoiceModal),
});

export default connect(mapStateToProps, mapDispatchToProps)(VoicesList);
