import React from "react";
import { connect } from "react-redux";
import { Route, RouteComponentProps } from "react-router-dom";

import { initialServers, initialCurrentServer } from "../redux/actions/servers";

import { AppThunkDispatch, RootState } from "../redux/store";

import Channels from "./components/Channels";
import { AddChannelModal } from "./components/AddChannelModal";
import { ServersSideBar } from "./components/ServerSideBar";

import { setOfflineUser, setOnlineUser } from "../api";
import { hideLoader, showLoader } from "../redux/actions/modals";
import { AddVoiceModal } from "./components/AddVoiceModal";

type MatchParams = {
  serverUID: string;
  chatUID: string;
};

interface MatchProps extends RouteComponentProps<MatchParams> {}

type ServersProps = ReturnType<typeof mapStateToProps> &
  ReturnType<typeof mapDispatchToProps> &
  MatchProps;

class Servers extends React.Component<ServersProps> {
  uploadSetOffline = (e: BeforeUnloadEvent) => {
    e.preventDefault();
    setOfflineUser(this.props.match.params.serverUID, this.props.user);
  };

  componentDidMount() {
    this.props.showLoader();
    this.props.initialServers(this.props.user.servers);
    this.props.initialCurrentServer(this.props.match.params.serverUID);
    setOnlineUser(this.props.match.params.serverUID, this.props.user);

    // set offline when user close browser tab
    window.addEventListener("beforeunload", this.uploadSetOffline, false);
  }

  shouldComponentUpdate(nextProps: ServersProps) {
    if (this.props.servers === nextProps.servers) {
      return (
        this.props.match.params.serverUID !== nextProps.match.params.serverUID
      );
    }

    return true;
  }

  componentDidUpdate(prevProps: ServersProps) {
    if (Object.keys(this.props.user).length) {
      this.props.hideLoader();
    }

    if (
      this.props.match.params.serverUID !== prevProps.match.params.serverUID
    ) {
      setOfflineUser(prevProps.match.params.serverUID, this.props.user);
      setOnlineUser(this.props.match.params.serverUID, this.props.user);
      this.props.initialCurrentServer(this.props.match.params.serverUID);
    }
  }

  componentWillUnmount() {
    setOfflineUser(this.props.match.params.serverUID, this.props.user);
    window.removeEventListener("beforeunload", this.uploadSetOffline, false);
  }

  render() {
    const { match, servers } = this.props;
    const { serverUID } = match.params;

    return (
      <div className="home">
        <ServersSideBar servers={servers} serverUID={serverUID} />

        <Route
          path={`/channels/${serverUID}/:chatUID`}
          render={() => <Channels serverUID={serverUID} />}
          exact
        />

        <AddChannelModal serverUID={serverUID} />
        <AddVoiceModal serverUID={serverUID} />
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  user: state.user.user,
  servers: state.servers.servers,
});

const mapDispatchToProps = (dispatch: AppThunkDispatch) => ({
  initialServers: (servers: string[]) => dispatch(initialServers(servers)),
  initialCurrentServer: (serverUID: string) =>
    dispatch(initialCurrentServer(serverUID)),
  showLoader: () => dispatch(showLoader),
  hideLoader: () => dispatch(hideLoader),
});

export default connect(mapStateToProps, mapDispatchToProps)(Servers);
