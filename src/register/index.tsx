import React from "react";
import { Link } from "react-router-dom";

import { RegisterForm } from "../components/RegisterForm";

const Register: React.FC = () => {
  return (
    <section className="auth__container">
      <div className="auth__box">
        <header className="auth__header">
          <h1 className="auth__title">Создать учётную запись</h1>
        </header>

        <RegisterForm />
        <footer className="auth__footer">
          <Link to="/login" className="auth__link">
            Уже зарегистрированы?
          </Link>
        </footer>
      </div>
    </section>
  );
};

export default Register;
