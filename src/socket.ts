import io from "socket.io-client";
import Peer from "simple-peer";

const socket = io.connect("https://dicord-clone-voice.herokuapp.com/");

const createPeer = (
  userToSignal: string,
  callerID: string,
  stream: MediaStream
) => {
  const peer = new Peer({
    initiator: true,
    trickle: false,
    stream,
    config: {
      iceServers: [
        { urls: "stun:stun.l.google.com:19302" },
        { urls: "stun:global.stun.twilio.com:3478?transport=udp" },
      ],
    },
  });

  peer.on("signal", (signal) => {
    socket.emit("sending signal", {
      userToSignal,
      callerID,
      signal,
    });
  });

  return peer;
};

const addPeer = (
  incomingSignal: Peer.SignalData,
  callerID: string,
  stream: MediaStream
) => {
  const peer = new Peer({
    initiator: false,
    trickle: false,
    stream,
    config: {
      iceServers: [
        { urls: "stun:stun.l.google.com:19302" },
        { urls: "stun:global.stun.twilio.com:3478?transport=udp" },
      ],
    },
  });

  peer.on("signal", (signal) => {
    socket.emit("returning signal", { signal, callerID });
  });

  peer.signal(incomingSignal);

  return peer;
};

export { addPeer, createPeer };

export default socket;
